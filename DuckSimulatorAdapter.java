public class DuckSimulatorAdapter implements Quackable {
    
    private RedDuck redDuck;
    private WhiteDuck whiteDuck;

    public DuckSimulatorAdapter(RedDuck redDuck){
        this.redDuck = redDuck;
    }

    public DuckSimulatorAdapter(WhiteDuck whiteDuck){
        this.whiteDuck = whiteDuck;
    }

    @Override
    public void quack() {     
        redDuck.quack(); 
        whiteDuck.quack();  
    }
}
